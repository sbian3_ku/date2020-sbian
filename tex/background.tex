\section{Preliminaries}\label{sec:background}
% \subsection{Notations}
% We use bold-face characters ($\ba$) to represent vectors, and capital letters
% ($A$) for matrices. $\lg x$ is the shorthand for $\log_2 x$.

\subsection{Cryptographic Constructions}\label{sec:pahe}
In this work, we mainly consider the optimization involving three cryptographic
primitives, packed additive homomorphic encryption (PAHE), garbled circuits
(GC), and secret sharing (SS). In what follows, we provide a high-level
abstraction of each individual primitive.

{\bf{PAHE}}: A PAHE is a cryptosystem, where the encryption ($\Enc$) and
decryption ($\Dec$) functions act as group (additive) homomorphisms between
the plaintext and ciphertext spaces. Except for the normal $\Enc$ and
$\Dec$, a PAHE scheme is equipped with the following three
abstract operators. Note that $n\in\zz$ is an integer.
\begin{itemize}
  \item Homomorphic addition $(\boxplus)$: for $\bx, \by\in\zz^{n}$, 
    $\Dec(\Enc(\bx)\boxplus\Enc(\by))=\bx+\by$.
  \item Homomorphic Hadamard product $(\boxcircle)$: for $\bx, \by\in\zz^{n}$,
    $\Dec(\ElementMult{\Enc(\bx)}{\by})=\bx\circ \by$, where $\circ$ is the
    element-wise multiplication operator.
  \item Homomorphic rotation $(\rot)$: for $\bx\in\zz^{n}$, let
  $\bx=[x_0, x_1, \cdots, x_{n-1}]$, 
    $\rot([\bx], k)=[x_{k}, x_{k+1}, \cdots, x_{n-1}, x_{0}, \cdots, x_{k-1}]$
  for $k\in\{0, \cdots, n-1\}$.
\end{itemize}
For most RLWE-based PAHE schemes, each homomorphic 
evaluation increases the internal error level contained in a ciphertext. When 
the size of the error become too large, {\it{some}} ciphertexts will not be 
correctly deciphered. We emphasize the point that not all ciphertexts become 
undecipherable, and this probabilistic behavior can be utilized to improve the 
efficiency of SI schemes.

{\bf{GC}}: GC can be considered as a more general form of HE. In
particular, the circuit garbler, Alice, ``encrypts'' some function $f$ along
with the inputs of $f$ to Bob, the circuit evaluator. Bob evaluates $f$ using
his encrypted inputs that is received from Alice obliviously, and obtains the
encrypted outputs. Alice and Bob jointly ``decrypt'' the output of the
function $f$. 
% Since GC guarantees security for an arbitrary Boolean circuit
% $f$, any GC can be seen as a type of PAHE (i.e., we can add, multiply and
% rotate the inputs to the circuits without decryption).

% {\bf{SS}}: In this work, we only consider the simplest SS scheme, i.e.,
%   additive SS (ASS). A two-party ASS scheme contains two operations, $s_{A},
%   s_{b} = \share(x) = (x - s_{B})\bmod{\pass}$ for $s_{A}, s_{B},
%   x\in\zz_{\pass}$, and $x = \rec(s_{A},
%   s_{B})=(s_{A}+s_{B})\bmod{\pass}$. The security property of SS is that,
%   given either (but not both) $s_{A}$ and $s_{B}$, we cannot recover $x$. 

\subsection{Secure Neural Network Inference}\label{sec:si}
\begin{figure}[!t]
 \centering
 \includegraphics[width=0.99\columnwidth]{fig/Motivation2.eps}
 \caption{An example of the architecture in Gazelle with one $\Conv$ layers,
   one non-linear layers and one $\FC$ layer.}
   %The $\FC$ layer, much like the $\Conv$ layers, is internally a homomorphic matrix-vector product.}
 \label{fig:gazelle_gen}
\end{figure}
While a number of pioneer works have already established the concept of secure
inference and training with neural networks~\cite{liu2017oblivious,
mohassel2017secureml, rouhani2018deepsecure}, it was not until recently that
such protocols carried practical significance. For example,
in~\cite{liu2017oblivious}, an inference with a single CIFAR-10 image takes
more than 500 seconds to complete. Using the same neural architecture, the
performance was improved to less than 13 seconds in one of the most recent
arts on SI, Gazelle~\cite{juvekar2018gazelle}. Unfortunately, 13 seconds per
image inference is obviously still unsatisfactory, especially given the large
amount of data exchange in real-world applications. Therefore, we adopt the
Gazelle protocol in this work, and take a system-level approach to improve its
efficiency. %TODO: 13 is not online time, should change to online

The general protocol and architecture of Gazelle is outlined in
Fig.~\ref{fig:gazelle_gen}, where Bob wants to classify some input (e.g.,
image), and Alice holds the weights. The threat model is that both Alice and
Bob are semi-honest, in the sense that both parties follow the described
protocol, but want to learn as much information as possible from the other
party. The Gazelle protocol classifies all NN operations into two types of
layers: i) linear layers, where the computations can be efficiently carried
out by HE-based cryptographic primitives, and ii) non-linear layers, where
interactive protocols such as multiplication triples~\cite{beaver1991efficient}
or garbled circuits~\cite{yao1982protocols} are employed.



% The basic approach (called na{\"i}ve method
% in~\cite{juvekar2018gazelle}) for computing an inner product is as follows. For 
% some row vector $\bw$ in the weight matrix $W$ and
% ciphertext $[\bu]$, we homomorphically compute the coefficient-wise product
% vector $[\bv]=[\bu]\boxdot \bw$, where each $v_{i}\in\bv$ satisfies $v_i =
% u_{i}\cdot w_{i}$ for $u_i\in\bu, w_i\in\bw$. Then, to obtain the sum
% $\sum_{i}^{n-1}v_{i}$, we i) create a rotated version of $[\bv]$ by a step
% size of $k=n/2$, and ii) compute a coefficient-wise homomorphic addition between
% $[\bv]$ and $\rot([\bv], k)$. Repeating i) and ii) $\log_{2}(n)$ times,
% each time decreasing the value of $k$ by half (i.e., $k_i=n/2^{i}$ for $i\in
% [1, \log_2(n)]\cap\zz$), we obtain the desired sum in
% the first plaintext slot.

% The shortcoming of the basic technique is that, for a weight matrix of
% dimension $n_o\times n$, computing $W\cdot [\bu]$ results in $n_o$ many
% ciphertexts, each containing only one result of the inner product, which blows
% up the communication bandwidth. 

\subsection{Neural Architecture Search}

Most recently, Neural Architecture Search (NAS) has been consistently achieving breakthroughs in different machine learning applications, such as image classifications \cite{zoph2016neural}, image segmentation \cite{liu2019auto}, video action recognition \cite{peng2019video}, etc.
NAS attracts large attentions mainly because it successfully releases human expertise and labor to identify high-accuracy neural architectures.

A typical NAS, such as that in \cite{zoph2016neural}, is composed of a controller and a trainer.
The controller will iteratively predicts neural architecture parameters, called child network, and the trainer will train the child network from scratch on a held-out data set to obtain its accuracy.
Then, the accuracy will be feedback to update controller.
Finally, after the number of child networks predicted by the controller exceed a predefined threshold, the search process will be terminated. 
The searched architecture with the highest accuracy will be finally identified.

Existing work has demonstrated that the automatically searched neural architectures can achieve close accuracy to the best human-invented architectures \cite{zoph2017learning,zoph2016neural}.
However, the identified architectures have complicated structures, which render them useless in real-world applications; for instance, it may result in excessive bandwidth requirement to perform secure inference.
