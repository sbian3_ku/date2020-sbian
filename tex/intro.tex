\section{Introduction}\label{sec:intro}
with the growing concerns over property stealing~\cite{juuti2019prada}
and private information leakage~\cite{shokri2017membership}, the study of the
security properties of both neural network (NN) training~\cite{liu2017oblivious} 
and inference~\cite{juvekar2018gazelle} is attracting increasing attentions. 
In particular, a secure
inference (SI) scheme refers to the situation where Bob as a client wants to
hide his inputs to Alice, the NN service provider. Meanwhile, Alice also needs to
protect her trained network model, as such a trained model is valuable considering the lengthy and costly training process.

While a number of protocols have been proposed for both secure inference and
training neural networks~\cite{liu2017oblivious, mohassel2017secureml, rouhani2018deepsecure, juvekar2018gazelle}, 
the general approach of existing works is
to find the equivalent NN operation (e.g., matrix-vector product, activation
functions) in secure domain (e.g., using garbled circuits or homomorphic
encryption), and instantiate the secure protocols accordingly. In other words,
security is not an integral part of the proposed protocol, but rather an added
feature with (in many cases serious) performance overheads.  


Recent advances
in the secure machine learning field have suggested the possibility of formulating the secure protocols as a design automation problem. 
For example,
in~\cite{bian2019darl}, authors proposed a framework that automatically
instantiate parameters for homomorphic encryption (HE) schemes. Likewise, a
line of research efforts~\cite{juvekar2018gazelle, jiang2018secure,
dathathri2019chet} have explored how to optimize HE parameters and packing
capabilities to improve the efficiency of secure computations, especially for
neural network based protocols. 
In all these works, secure primitives are designed to maximize efficiency 
of a pre-trained, well-defined neural architecture (in fact, many of
the existing works use the same manually designed architecture).
% A recent work also explores how secure
% protocols can be optimized when binary neural networks are
% adopted~\cite{riazi2019xonn}.
% Weiwen

% As we can observe, the common theme to the
% existing works are that given a pre-trained, well-defined neural network
% architecture, secure primitives are designed to maximize either the training
% or the inference efficiency.


We argue that the existing design techniques based on a fixed neural architecture 
will lead to unsatisfactory solutions, because securer inference efficiency 
(both the inference time and network bandwidth) are significantly affected by the neural architectures. 
This can be clearly demonstrated through Fig.~\ref{fig:motivation}, where the computational and
communication costs of SI are plotted with respect to quantization factors (Fig.~\ref{fig:motivation}(a))
and filter hyperparameters (Fig.~\ref{fig:motivation}(b)) of a neural architecture.
From Fig.~\ref{fig:motivation}(a), we can see that for certain quantization intervals
(e.g., 14-bit to 15-bit), the inference time is doubled, while for other intervals (e.g., from 2-bit up to
15 bits), the inference time remains unchanged. This is primarily due to the fact that the underlying 
primitive (here a packed homomorphic encryption (PAHE) scheme) is constrained by its cryptographic
parameters, resulting in an extremely non-linear performance curve.
A similar behavior is observed in Fig.~\ref{fig:motivation}(b), where increasing the filter channels 
also produces a staircase-like performance characteristics in terms of the network bandwidth.
Our conclusion here is that, in order to obtain better design trade-offs, a joint 
exploration considering both secure primitives and neural 
architectures is required to push forward the Pareto frontier of both the efficiency and prediction 
accuracy of NN-based SI. % TODO: not sure if wording is correct for Pareto  frontier of efficiency & accuracy



% clearly demonstrate that both inference time and network bandwidth are significantly affected by the neural architectures.

In this work, we propose a novel neural architecture search framework, named {\placeholder}, that 
integrates the optimization of secure primitives and NN prediction accuracy.
Specifically, {\placeholder}~takes a {\it{synthetic}} approach to improve the computational and bandwidth
efficiency of SI.
We realize that SI should be considered as an integrated system, and the system optimizer, in our case a 
neural architecture search (NAS) engine, needs to take feedback from the security designs of the protocol. 
As a result, in {\placeholder}, accuracy, security and
efficiency (for both computations and communications) are jointly optimized to determine the best
architecture for SI. Experimental results demonstrate that compared to
the baseline architecture that is manually crafted~\cite{liu2017oblivious,
juvekar2018gazelle}, the co-optimized architecture achieves 1.7\% accuracy gain
while reducing the computational and bandwidth costs by 1.46x and 1.40x, respectively.
Our main contributions are summarized as follows.

\begin{figure}[!t]
 \centering
 \includegraphics[width=0.99\columnwidth]{fig/Motivation.eps}
 \vspace{-20pt}
 \caption{Neural architectures will significantly affect the optimal performance of secure inference, using a network with 1 Conv, 1 Relu, and 1 FC as example: (a) one more bit may double inference time; (b) one more channel may dramtically increase bandwidth requirement.}
 \label{fig:motivation}
\end{figure}


\begin{itemize}
 \setlength{\itemsep}{0pt}
  \item {\bf{Synthesizing Secure Architectures}}:
    To the best of our knowledge, {\placeholder} represents the first work to
    search for neural architectures optimized in secure applications.
    Cryptographic primitives are modeled as design elements, and secure
    computations with these elements become abstract operators that can be fed
    to optimization engines. 
    
  \item {\bf{Optimizing HE Parameters}}:
    While existing works have already treated the instantiation of HE
    parameters as a design problem and proposed corresponding
    solutions~\cite{bian2019darl}, we point out that parameter instantiation can be a subtle
    optimization problem. In particular, we identify the subtle parameter
    optimization dilemma that exists in learning with errors (LWE) based
    cryptosystems, and observe that this optimization problem is
    (computationally) rather difficult to solve, especially for NAS-based
    optimization with fast turnaround time. As a solution, we provide
    heuristics for the fast search of HE parameters with different levels of
    efforts.

  \item {\bf{A Thorough Architectural Search for SI}}:
    By conducting extensive architectural search, it is demonstrated that
    the performance of SI can be reduced while improving the prediction accuracy.
    We achieve a prediction accuracy of 83.3\% on the CIFAR-10 dataset, 
    while reducing 1.46x computational time and 1.40x network bandwidth, 
    compared to the best known SI scheme~\cite{juvekar2018gazelle}.
\end{itemize}

The rest of this paper is organized as follows. First, in
Section~\ref{sec:background}, basics on PAHE, secure inference, and NAS
are discussed.  {\song{Second, the {\placeholder} framework is outlined
in Sections~\ref{sec:framework} and~\ref{sec:propose}  , where we 
detail how security and parameter analysis can
be systematically performed, with derivation of reward functions for the
integration with NAS engines.}} Next, the output architectures of {\placeholder}
along with performance statistics are demonstrated in
Section~\ref{sec:experiment}.  Finally, our work is summarized in
Section~\ref{sec:conclusion}.
