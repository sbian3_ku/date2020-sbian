

\section{Estimators for Cryptographic Primitives}\label{sec:propose}
While CHET~\cite{dathathri2019chet} realizes the importance of establishing an
abstraction layer for the NN designer to hide the specific HE implementation
details, they did not think of the cryptographic primitives as {\it{design
elements}} that carry distinct performance trade-offs (actually, CHET only
focused on compiling FHE schemes). In this section, we describe how to
construct estimators that model cryptographic primitives as delay elements
with communicational costs, analogous to the FPGA components modeled in the
FNAS~\cite{jiang2019accuracy} framework.

As observed in Fig.~\ref{fig:gazelle_gen}, Gazelle instantiate different
protocols according to the specific computations involved in NN layers. 
In what follows, we describe how the characterization
engines can be designed to automatically evaluate the performance of
cryptographic primitives given a specific neural architecture.

% \subsection{Overall Estimator Architecture}
% \begin{figure}[!t]
%  \centering
%  \includegraphics[width=0.85\columnwidth]{./fig/nass_est_gen.pdf}
%  \caption{An overview of the {\placeholder} estimator architecture.}
%  \label{fig:nass_est_gen}
% \end{figure}


% The overview of the proposed estimator is sketched in
% Fig.~\ref{fig:nass_est_gen}, where the inputs to the estimator is the network
% architecture. The neural architecture consists of a set of layer
% architectures, where each layer architecture contains the corresponding input
% and output parameters. It is noted that for different layer, the parameters are
% also different. 
% \begin{itemize}
% \item For linear layers, the
%   parameters set contains image dimensions
%   $n_{i}, n_{o}$, filter dimensions $f_{w}, f_{h}$, input quantizer $l_{i}$,
%   filter quantizer $l_{f}$, input channel number $c_{i}$, and output channel
%   number $c_{o}$. We refer to this parameter set as $\layerparml=\{n_{i}, n_{o},
%   f_{w}, f_{h}, l_{i}, l_{f}, c_{i}, c_{o}\}$, 
% \item For non-linear layers, the parameters set contains only the input
%   dimensions $n_{i}, n_{o}$, input channel number $c_{i}$, input quantizer
%   $l_{i}$, and output quantizer $l_{o}$, and we have that $\layerparmnl=\{n_{i},
%   n_{o}, l_{i}, l_{o}, c_{i}\}$. %Note that since
% \end{itemize}
% and that the collection of parameters $\networkparm=\{\layerparm_{L, i},
% \layerparm_{NL, i}\}$ for $i=0, 1, \cdots$.  The parameters are fed to two
% separate engines, the parameter instantiation engine (PIE), and the
% performance characterization engine (PCE). The outputs of PIE are the
% cryptographic parameters. For PAHE-based protocols (e.g., homomorphic
% matrix-vector product in linear layers and multiplication triples for square
% activation), the parameters are $(n, q, p)$, and a detailed analysis is
% conducted in Section~\ref{sec:feedback}. For activation functions such as ReLU
% that relies on the GC protocol, the parameters can be determined solely based
% on the number of inputs and the quantizer contained in $\layerparmnl$, and
% a PIE is not needed.

% Upon the inputs of LWE parameters and layer architecture, PCE performs
% consultations with HE and GC libraries, and produces characterized scores for
% a single round of secure inference using the architecture specified in
% $\networkparm$.  The output scores will later be used for evaluating and
% improving the neural architectures.

\subsection{PIE for PAHE and The Feedback Loop}\label{sec:feedback}
In this work, we use the widely-adopted BFV~\cite{fan2012somewhat}
cryptosystem as the example PAHE scheme, but our method applies broadly to
RLWE-based PAHE cryptosystems.  In BFV, three parameters are required to
instantiate, $(n,
p, q)$, where $n$ is the lattice dimension, $p$ the plaintext modulus, and $q$
the ciphertext modulus. 

% The main difference between the proposed PIE and CHET~\cite{dathathri2019chet}
% is that, we have an stronger demand for smaller parameters to ensure efficient
% inference execution. Since CHET targets on FHE-based secure inference schemes,
% their parameter sets are much larger than that of Gazelle, especially when the
% neural architecture gets deeper and more complex. For example, CHET needs
% $n=8192$ and a ciphertext modulus $q$ of 240 bits for two convolutional layers
% and two fully-connected layers, but the parameters scale to an $n=32768$ and a
% $q$ of 705 bits to evaluate an NN with five convolutional layers. By using a
% large parameter set, the error analysis can be somewhat loose, as even 10 bits
% of error margin do not degrade the performance that much (only increases the
% modulus size by less than 1.5\%).  In contrast, Gazelle kept the parameters
In the Gazelle protocol, parameters are minimized, where $n=(2048, 4096)$, 
and $q$ from 60 to 120 bits. Therefore, even one
bit of loose error margin can easily result in 1.5x to 2x performance penalty
on 64-bit machines due to the requirement of extra integer slots (e.g., from 61-bit 
$q$ to 62-bit $q$). This is
because of the fact that Gazelle evaluates each convolutional layer
independently, and as long as the dimensions and quantizers are the same, the
parameters do not scale (i.e., the same $n$ and $q$ can be used even if the
network's number of convolutional layers increase indefinitely).  Therefore,
the minimization of parameter expansion for every NN layer needs to be
carried out, and the architecture for each varies significantly.

To perform a per-layer parameter instantiation, the main difficulty lies in
the feedback loop between PCE and PIE. The dilemma is that, in order for PIE
to instantiate parameters that ensure correct decryption, the error size needs
to be estimated by PCE.  Meanwhile, PCE needs instantiated PAHE parameters
from PIE to perform error analysis, resulting in a feedback loop. Automating
the parameter generation and selection processes create significant
computational burden, as specific constraint on the relationship of $p$, $q$
and $n$ needs to be satisfied for RLWE-based cryptosystems to operate
efficiently with the batching technique~\cite{smart2010fully} enabled. 
In particular, both $p$ and $q$ need to satisfy $p\equiv q\equiv 1 
(\bmod{\hspace{0.3em}n})$,  where $q$ can be a large integer 
(e.g., 120 bits).

\begin{figure}[!t]
 \centering
 \includegraphics[width=1\columnwidth]{./fig/nass_est_overview_ww.eps}
 \vspace{-20pt}
 \caption{The PIE-PCE co-optimization procedures for characterizing 
 the performance cost of a linear layer.}
 \label{fig:pie_pce}
\end{figure}

An overview of the joint parameter optimization procedure 
is illustrated in Fig.~\ref{fig:pie_pce}.

{\large\ding{192}} {\bf{Initialization}}: To start the optimization process, inputs are first fed to PIE. The inputs include 
$n_{0}$, the initial lattice dimension, and ($l_{i}$, $l_{f}$), the respect 
quantizers for NN inputs and filters. Here, $n_{0}$ is an arbitrary number, and 
can be set as the smallest $n$ that grants some security levels for extremely 
small $q$ (e.g., $n_{0}=1024$, which is secure for $q\leq 32$). $l_{i}$ and 
$l_{f}$ is used to determine the plaintext modulus $p$. In order to carry out a
successful inference, we need that $p\geq l_{i}+l_{f}+\lceil\log_{2}(f_{h}\cdot 
f_{w})\rceil$ and $p\equiv 1(\bmod{\hspace{0.3em}n_{0}})$. After generating the 
plaintext modulus $p$, along with $n_{0}$ and other parameters in $\layerparmnl$, 
we can calculate a working ciphertext modulus $q$. Note that this estimation can
be loose, but will be tightened in PCE through optimization iterations.

{\large\ding{193}} {\bf{Optimization Loop}}: Upon receiving parameters from PIE, PCE performs two
important evaluations: i) security level estimation, and ii) decryption failure rate 
estimation. Failure in meeting either of the conditions results in an immediate 
rejection. The security levels are consulted with the community standard established 
in~\cite{HomomorphicEncryptionSecurityStandard}. When the security 
standard is not met, we regenerate the lattice dimension $n$ and retry the security analysis. 
After obtaining a valid $n$ for the estimated $q$, a set of ciphertexts are created 
to see if $q$ is large enough for correct decryption. % We postpone
% a more detailed analysis on $q$ in Section~\ref{sec:ciph_modulus}, and only note that
If the decryption failure rate is too high, we regenerate a larger $q$ and re-evaluate 
the security of $n$ with respect to the new $q$. After deriving valid $(n, p, q)$ that
passes all the tests, the parameters are fed into a PAHE library to characterize the
estimated amount of time and memory consumed by a single layer to calculation.

{\large\ding{194}} {\bf{Output Statistics}}: Step 1 and 2 described above will be repeated for every 
layer in the input neural architecture, and all performance statistics are summed up
to produce a final score to be used by the overall NASS framework in searching for a 
better neural architecture for SI.

% \subsection{Generating a Valid Ciphertext Modulus}\label{sec:ciph_modulus}
% Two important observations can be made while generating the ciphertext modulus $q$. 
One last note on the ciphertext modulus $q$ is that, as mentioned in 
Section~\ref{sec:pahe}, not all ciphertexts become undecipherable when $q$ is small. 
The probability that a ciphertext becomes undecipherable is called the decryption
failure rate. Observe that different from~\cite{bian2019darl}, we do not need an 
expensive simulation to ensure the asymptotically small (e.g., $2^{-40}$) decryption
failure probability, since NN-based SI mispredicts much more often than $2^{-40}$.
Therefore, we can use the standard Monte-Carlo simulation technique to ensure that 
$q$ is large enough to ensure that $\Pr[\Enc(\Dec(m))\not=m]<\delta$, where $\delta$
ranges from $10^{-3}$ to $10^{-2}$, depending on the prediction accuracy requirement.
% Second, it is also noted that, if $q$ is not large enough, we need to regenerate 
% a larger $q$. Since $q$ can be relatively large (120 bits), performing an exhaustive 
% search on  the smallest prime $q$ that tolerates 
% the evaluation errors and splits $n$ (i.e., $q\equiv 1(\bmod{\hspace{0.3em}n_{0}})$)
% can be time consuming. In this work, we generate $q$ in two approaches. 
% which may result in a failed security test, further increasing the size of $n$.

\subsection{PCE: Performance Characterization}
\subsubsection{Characterizing Linear Layers}\label{sec:pce_lin}
The main arithmetic computations in both
$\Conv$ and $\FC$ involve a set of inner products between some plaintext
matrix and some ciphertext matrix (both flatten as vectors) homomorphically.
The so called hybrid approach proposed in~\cite{juvekar2018gazelle} aligns
the weight matrix with the rotating input ciphertext (instead of the rotating
product ciphertext as in the basic approach).  In summary, the hybrid approach
computes $W\boxdot[\bu]$ as follows.
\begin{align}
  [\bt] &= \sum_{i=0}^{n_o-1}\bw_i\boxdot \rot([\bu], i)\label{eq:gaz_eval}\\
        &=\bw_0\boxdot [\bu] + \cdots + \bw_{n_o -1}\boxdot \rot([\bu],
  n_o-1),\label{eq:hybrid_1}\\
  [\bv] &= \sum_{i=1}^{\lg{(n/n_o)}}\rot\left([\bt],
  \frac{n}{2^i}\right),\label{eq:hybrid_2}
\end{align}
where $[\br]$ holds the result vector $\br=W\bv\in\zz^{n_o}_{p}$, $\bw_{i}$'s
are the diagonally aligned columns of $W$ with dimension
$\bw_{i}\in\zz^{n}_{p}$, and $\lg{(\cdot)}$ denotes $\log_{2}{(\cdot)}$.  In
the hybrid approach shown in Eq.~\eqref{eq:hybrid_1}, we first rotate $[\bu]$
$n_o$ times, each time multiplying it with the aligned vectors
$\bw_i\in\{\bw_{0}, \cdots, \bw_{n_o-1}\}$. Each multiplication generates an
intermediate ciphertext that holds only {\it{one}} entry in $\bv_{i}$ with respect to
$\bw_{i}$.  Summing these ciphertexts gives us a single ciphertext that
is packed with $n/n_o$ partial sums in the corresponding inner products, 
and packed results can be summed up to obtain the final product.

It is noted that actual library implementations are needed for both decryption
failure rate estimation and performance statistics estimation, in order to 
obtain tight parameters running on actual machines.
The parameters in $\layerparml$ along with the cryptographic 
parameters $(n, q, p)$ can be used to create simulation ciphertexts using 
actual PAHE libaries, such as SEAL~\cite{sealcrypto}, and the corresponding 
calculations can be implemented. After obtaining the inference running time, $\tau$,
and network bandwidth, $\nu$, we use a simple weighted sum to derive the performance 
score $\xi=\beta\cdot \tau + (1-\beta)\cdot \nu$.
One important 
implementation detail is that, instead of performing the entire calculation 
using the PAHE library, only basic operations ($\boxplus$, $\boxcircle$ and 
$\rot$) need to be characterized. The actual runtime and bandwidth usage can be
scaled from the combinations of the basic operations. In fact, this can be a critical 
performance improvement, as secure inference is still quite slow for deep neural 
networks (10 to 100 seconds), and running PCE for full characterizations can be a 
performance bottleneck in optimizing these network  architectures.


\subsubsection{PCE for Non-Linear Layers}
Running PCE for interactive protocols such as multiplication triples~\cite{beaver1991efficient}
and GC~\cite{yao1982protocols} is much simpler than linear layers, as these non-linear functions 
(e.g., square and ReLU) are performed on a per-element basis with fixed
functionality. The performance statistics can be characterized once, and used throughout all
layers when properly scaled.