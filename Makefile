TEXFILE = main.tex
TEXFILES = $(wildcard tex/*.tex)
BIBFILES = $(wildcard *.bib)

#TEX2DVI = /home/sbian/private/bin/latexmk -f
TEX2DVI = latexmk

EPSDIR = fig
EPSFILES = $(wildcard $(EPSDIR)/*.eps)

PAPERSIZE = letter

DVI2PDF = dvipdfmx

TEX2HTML = latex2html -split 0

.SUFFIXES: .tex .eps .obj .dvi .ps .ps.gz .pdf .html .zip

all: image 
	make pdf

.tex.dvi:
	$(TEX2DVI) $<

.dvi.pdf:
	$(DVI2PDF) -p $(PAPERSIZE) $<

.tex.html:
	$(TEX2HTML) $<

dvi: $(TEXFILE:.tex=.dvi)
$(TEXFILE:.tex=.dvi): $(TEXFILES) $(EPSFILES) $(BIBFILES)
pdf: $(TEXFILE:.tex=.pdf)
$(TEXFILE:.tex=.pdf): $(TEXFILES) $(EPSFILES) $(BIBFILES)
html: dvi $(TEXFILE:.tex=.html)
$(TEXFILE:.tex=.html): $(TEXFILES) $(EPSFILES) $(BIBFILES)
image:
	cd fig


clean:
	rm -fv *.bbl *.blg *.aux *.log *.toc *.fff *.brf *.lof *.out
	rm -fv *.dvi *.ps
	rm -fv *.fdb_latexmk *.fls
	rm -fv $(TEXFILE:.tex=.pdf)
	rm -fv $(TEXFILE:.tex=.html)
